# All4Class - Plataforma testada para comunicação entre comunidade escolar e órgãos públicos

## Repositório de testes automatizados
Aqui encontra-se o código fonte usado para testar a aplicação, o repositório do código fonte da aplicação pode ser acessado [aqui](https://gitlab.com/senac-tcc-2019/all4class/projetodesenvolvimento-michelluz/blob/master/README.md)

## Descrição do Sistema
  O All4Class é uma plataforma pública para aproximar as comunidades escolares carentes e os órgãos públicos.
  Através do All4Class é possível criar requisições relatando vulnerabilidades e problemas que os alunos de uma escola nessa comunidade estão suscetíveis. Essas requisições são direcionadas à parte administrativa da plataforma, que aciona os órgãos solicitados para estrategicamente trabalhar nesses locais a fim de estancar a problemática no local. O All4Class auxilia no processo de criação de estratégia de atuação nessas comunidades por parte dos órgaos públicos através da geração de relatórios das requisições, mostrando através de gráficos e filtros quais escolas enfrentam maiores dificuldades e do que essa comunidade mais precisa. 


## Para rodar os testes

1. Abra sua IDE, de preferência [Eclipse](https://www.eclipse.org/downloads/)

2. Clique em **File->Import->Git->Projects from Git->Clone URI**
3. Cole o endereço do repositório no campo "URL": 
    
    **SSH**
    ```git
    git@gitlab.com:senac-tcc-2019/all4class/test.git
    ```

    **HTTPS**
    ```git
    https://gitlab.com/senac-tcc-2019/all4class/test.git
    ```

4. Após clonar o projeto, você já terá todas as bibliotecas e arquivos necessários para rodar os testes
5. Expanda a pasta do projeto e abra a pasta **config.properties**
6. Na linha que diz **Adress** coloque o endereço da aplicação no Heroku, para não precisar rodar local, já que você talvez não tenha o código fonte em sua máquina
    **HEROKU**
    ```git
    https://all4class.herokuapp.com/
    ``` 

7. Após salvar o  arquivo, abra o arquivo **testFile.java**
8. Clique com o botão direito nele e vá em **Run as->Junit Test**
9. Pronto, o arquivo irá abrir o Google Chrome e navegar até a aplicação, realizando alguns testes
10. Esses testes podem ser verificados após o navegador fechar, de volta no Eclipse, na aba **Junit**

## Desenvolvido por
 * [Michel Luz](@mortalis)

