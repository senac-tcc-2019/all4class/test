package build;

import org.openqa.selenium.WebDriver;

import page.admin.HomeAdminFormPage;
import page.publico.InicialFormPage;
import page.publico.LoginFormPage;
import util.Util;

import static util.Assertions.assertNotNull;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
public class AbstractTestCase {
	public static WebDriver driver;
	public static Properties properties = new Properties(); 
	public static boolean isSuit;
	public static String path;
	public static InicialFormPage inicio;
	public static LoginFormPage loginPage;
	public static HomeAdminFormPage homeAdmin;
	
	
	
	public AbstractTestCase(String name) {
		super();
	}
	
	@BeforeClass
	public static void initialize(){
		
		if (driver == null) {
			try {
				properties.load(new FileInputStream("src/config.properties"));

				driver = Util.initialize(properties.getProperty("address"));
			} catch (IOException e) {
				System.out.println("Não foi possível ler o arquivo de configuração, abrindo o Heroku");
				e.printStackTrace();
				driver = Util.initialize("https://all4class.herokuapp.com/");
			}
			Util.wait(5000);
			inicio = new InicialFormPage();
			loginPage = inicio.goToLoginPage();
			assertNotNull("Não está na página de login", loginPage);
			Util.wait(4000);
			assertNotNull("Não achei o campo de email", loginPage.setLogin("michelcluz@gmail.com"));
			Util.wait(4000);
			assertNotNull("Não achei o campo de senha", loginPage.setSenha("4815162342"));
			Util.wait(4000);
			assertNotNull("Não achei o botão de login", homeAdmin = loginPage.clickLogarButton());
			Util.wait(5000);
			
		}
		
		
		
		
	}
	
	public AbstractTestCase() {
		super();

	}
	
	@AfterClass
	public static void closeDriver() {
		if (!isSuit) {
			Util.takeScreenshot("before close");
			driver.close();
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			// driver.quit();

		}
	}
}
