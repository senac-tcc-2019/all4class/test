package entity.form;

import java.util.ArrayList;

import util.ComparableForm;

	final class Bairro extends ComparableForm{
		String nome;
	}
	
	public class BairroForm extends ComparableForm{
		private ArrayList<Bairro> bairros;
		
		public ArrayList<Bairro> getBairros() {
			return bairros;
		}

		public void setBairros(ArrayList<Bairro> bairros) {
			this.bairros = bairros;
		}
		
		public BairroForm() {
			bairros = new ArrayList<Bairro>();
		}

		public void addBairro(String nome) {
			Bairro bairro= new Bairro();
			bairro.nome = nome;
			bairros.add(bairro);
		}
		
		public String getBairroNome(int index) {
			return bairros.get(index).nome;
		}
	}
	
	