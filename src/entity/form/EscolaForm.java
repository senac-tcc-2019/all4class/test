package entity.form;

import java.util.ArrayList;

import util.ComparableForm;

	final class Escola extends ComparableForm{
		String nome, bairro, endereco, contato; 
	}
	
	public class EscolaForm extends ComparableForm{
		private ArrayList<Escola> escolas;
		
		public ArrayList<Escola> getEscolas() {
			return escolas;
		}

		public void setEscolas(ArrayList<Escola> escolas) {
			this.escolas = escolas;
		}

		public EscolaForm() {
			escolas = new ArrayList<Escola>();
		}

		public void addEscola(String nome, String bairro, String endereco, String contato) {
			Escola escola = new Escola();
			escola.nome = nome;
			escola.bairro = bairro;
			escola.endereco = endereco;
			escola.contato = contato;
			escolas.add(escola);
		}
		
		public String getEscolaNome(int index) {
			return escolas.get(index).nome;
		}

		public String getEscolaBairro(int index) {
			return escolas.get(index).bairro;
		}

		public String getEscolaEndereco(int index) {
			return escolas.get(index).endereco;
		}
		
		public String getEscolaContato(int index) {
			return escolas.get(index).contato;
		}

	}


