package page.admin;

import static org.junit.Assert.assertNotNull;

import java.util.NoSuchElementException;

import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.WebElement;

import com.gargoylesoftware.htmlunit.javascript.host.Element;

import build.AbstractTestCase;
import page.admin.form.BairroAdminForm;
import page.publico.LoginFormPage;

public class BairroAdminFormPage extends AbstractTestCase{
	WebElement bairroAdmin;
	public BairroAdminFormPage(){
		super();
		bairroAdmin = driver.findElement(By.xpath("/html/body/div"));
		assertNotNull("Não está na página de lista de bairros", bairroAdmin.findElement(By.id("BairroList")));
	}
	
	public LoginFormPage clickLogoutButton(){
		try{
			bairroAdmin.findElement(By.xpath("/html/body/div/header/nav/div/ul/li/a")).click();
		}catch(NoSuchElementException | ElementNotVisibleException e){
			return null;
		}return new LoginFormPage();
	}
	
	public EscolaAdminFormPage clickEscolaListButton(){
		try{
			bairroAdmin.findElement(By.xpath("/html/body/div/aside/section/ul/li[1]/a/span")).click();
		}catch(NoSuchElementException | ElementNotVisibleException e){
			return null;
		}return new EscolaAdminFormPage();
	}
	
	public BairroAdminFormPage clickBairroListButton(){
		try{
			bairroAdmin.findElement(By.xpath("/html/body/div/aside/section/ul/li[2]/a/span")).click();
		}catch(NoSuchElementException | ElementNotVisibleException e){
			return null;
		}return new BairroAdminFormPage();
	}
	
	public InstituicaoAdminFormPage clickInstituicaoListButton(){
		try{
			bairroAdmin.findElement(By.xpath("/html/body/div/aside/section/ul/li[3]/a")).click();
		}catch(NoSuchElementException | ElementNotVisibleException e){
			return null;
		}return new InstituicaoAdminFormPage();
	}
	
	public VulnerabilidadeAdminFormPage clickVulnerabilidadeListButton(){
		try{
			bairroAdmin.findElement(By.xpath("/html/body/div/aside/section/ul/li[5]/a/span")).click();
		}catch(NoSuchElementException | ElementNotVisibleException e){
			return null;
		}return new VulnerabilidadeAdminFormPage();
	}
	
	public RequisicaoAdminFormPage clickRequisicaoListButton(){
		try{
			bairroAdmin.findElement(By.xpath("/html/body/div/aside/section/ul/li[6]/a/span")).click();
		}catch(NoSuchElementException | ElementNotVisibleException e){
			return null;
		}return new RequisicaoAdminFormPage();
	}
	
	public String getBairroNome(int index){
		String nome;
		try{
			nome = bairroAdmin.findElement(By.xpath
					("/html/body/div/div/section[2]/div[4]/table/tbody/tr["+(index+1)+"]/td[2]")).getText();
		}catch(NoSuchElementException | ElementNotVisibleException e){
			return null;
		}return nome;
	}
	
	public BairroAdminForm clickEditButton(int index){
		try{
			bairroAdmin.findElement(By.xpath
					("/html/body/div/div/section[2]/div[4]/table/tbody/tr["+(index+1)+"]/td[3]/form[1]")).click();
		}catch(NoSuchElementException | ElementNotVisibleException e){
			return null;
		}return new BairroAdminForm();
	}
	
	public int getNumberOfBairros(){
		int numberOfBairros;
		try{
			numberOfBairros = bairroAdmin.findElements(By.xpath("/html/body/div/div/section[2]/div[4]/table/tbody/tr")).size();
		}catch (NoSuchElementException | ElementNotVisibleException e){
			return 0;
		}return numberOfBairros;
	}
}
