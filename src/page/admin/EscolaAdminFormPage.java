package page.admin;

import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.NoSuchElementException;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import build.AbstractTestCase;
import entity.form.EscolaForm;
import page.admin.form.EscolaAdminForm;
import page.publico.LoginFormPage;

public class EscolaAdminFormPage extends AbstractTestCase{
	WebElement escolaAdmin;
	public EscolaAdminFormPage(){
		super();
		escolaAdmin = driver.findElement(By.xpath("/html/body/div"));
		assertNotNull("Não está na página de lista de escolas", escolaAdmin.findElement(By.id("EscolaList")));
	}
	
	public LoginFormPage clickLogoutButton(){
		try{
			escolaAdmin.findElement(By.xpath("/html/body/div/header/nav/div/ul/li/a")).click();
		}catch(NoSuchElementException | ElementNotVisibleException e){
			return null;
		}return new LoginFormPage();
	}
	
	public EscolaAdminFormPage clickEscolaListButton(){
		try{
			escolaAdmin.findElement(By.xpath("/html/body/div/aside/section/ul/li[1]/a/span")).click();
		}catch(NoSuchElementException | ElementNotVisibleException e){
			return null;
		}return new EscolaAdminFormPage();
	}
	
	public BairroAdminFormPage clickBairroListButton(){
		try{
			escolaAdmin.findElement(By.xpath("/html/body/div/aside/section/ul/li[2]/a/span")).click();
		}catch(NoSuchElementException | ElementNotVisibleException e){
			return null;
		}return new BairroAdminFormPage();
	}
	
	public InstituicaoAdminFormPage clickInstituicaoListButton(){
		try{
			escolaAdmin.findElement(By.xpath("/html/body/div/aside/section/ul/li[3]/a")).click();
		}catch(NoSuchElementException | ElementNotVisibleException e){
			return null;
		}return new InstituicaoAdminFormPage();
	}
	
	public VulnerabilidadeAdminFormPage clickVulnerabilidadeListButton(){
		try{
			escolaAdmin.findElement(By.xpath("/html/body/div/aside/section/ul/li[5]/a/span")).click();
		}catch(NoSuchElementException | ElementNotVisibleException e){
			return null;
		}return new VulnerabilidadeAdminFormPage();
	}
	
	public RequisicaoAdminFormPage clickRequisicaoListButton(){
		try{
			escolaAdmin.findElement(By.xpath("/html/body/div/aside/section/ul/li[6]/a/span")).click();
		}catch(NoSuchElementException | ElementNotVisibleException e){
			return null;
		}return new RequisicaoAdminFormPage();
	}
	
	public EscolaForm getEscolaListInfo(){
		EscolaForm escolaForm = new EscolaForm();
		for (int i=0; i<getNumberOfEscolas(); i++){
			escolaForm.addEscola(getEscolaNome(i), getEscolaBairro(i), getEscolaEndereco(i), getEscolaContato(i));
		}
		
		return escolaForm;
	}
	
	public String getEscolaNome(int index){
		String nome;
		nome = escolaAdmin.findElement(
				By.xpath("/html/body/div/div/section[2]/div[1]/table/thead[2]/tr["+(index+1)+"]/td[1]")).getText();
		return nome;
	}
	
	public String getFirstEscolaNome(){
		String nome;
		nome = escolaAdmin.findElement(
				By.xpath("/html/body/div/div/section[2]/div[1]/table/thead[2]/tr/td[1]")).getText();
		return nome;
	}
	
	public String getEscolaBairro(int index){
		String bairro;
		bairro = escolaAdmin.findElement(
				By.xpath("/html/body/div/div/section[2]/div[1]/table/thead[2]/tr["+(index+1)+"]/td[2]")).getText();
		return bairro;
	}
	
	public String getEscolaEndereco(int index){
		String endereco;
		endereco = escolaAdmin.findElement(
				By.xpath("/html/body/div/div/section[2]/div[1]/table/thead[2]/tr["+(index+1)+"]/td[3]")).getText();
		return endereco;
	}
	
	public String getEscolaContato(int index){
		String contato;
		contato = escolaAdmin.findElement(
				By.xpath("/html/body/div/div/section[2]/div[1]/table/thead[2]/tr["+(index+1)+"]/td[4]")).getText();
		return contato;
	}
	
	public int getNumberOfEscolas(){
		int number;
		try{
			number = escolaAdmin.findElements(By.xpath("/html/body/div/div/section[2]/div[1]/table/thead[2]/tr")).size();
		}catch(NoSuchElementException | ElementNotVisibleException e){
			return 0;
		}
		return number;
	}
	
	public EscolaAdminForm clickEditButton(int index){
		try{
			escolaAdmin.findElement(By.xpath
					("/html/body/div/div/section[2]/div[1]/table/thead[2]/tr["+(index+1)+"]/td[5]/form[1]")).click();
		}catch(NoSuchElementException | ElementNotVisibleException e){
			return null;
		}return new EscolaAdminForm();
	}
	
	public EscolaAdminFormPage setNomeFiltro(String nome){
		try{
			escolaAdmin.findElement(By.xpath("/html/body/div/div/section[2]/form/input[2]")).sendKeys(nome);
		}catch(NoSuchElementException | ElementNotVisibleException e){
			return null;
		}return new EscolaAdminFormPage();
	}
	
	public EscolaAdminFormPage setBairroFiltro(int index){
		try{
			Select select = new Select(escolaAdmin.findElement(By.xpath("//*[@id='bairro']")));
			select.selectByIndex(index);
		}catch(NoSuchElementException | ElementNotVisibleException e){
			return null;
		}return new EscolaAdminFormPage();
	}
	
	public EscolaAdminFormPage setContatoFiltro(String nome){
		try{
			escolaAdmin.findElement(By.xpath("/html/body/div/div/section[2]/form/input[3]")).sendKeys(nome);
		}catch(NoSuchElementException | ElementNotVisibleException e){
			return null;
		}return new EscolaAdminFormPage();
	}
	
	public EscolaAdminFormPage clickFiltrarButton(){
		try{
			escolaAdmin.findElement(By.xpath("/html/body/div/div/section[2]/form/button")).click();
		}catch(NoSuchElementException | ElementNotVisibleException e){
			return null;
		}return new EscolaAdminFormPage();
	}
	
	public EscolaAdminForm clickAddEscolaButton(){
		try{
			escolaAdmin.findElement(By.xpath("//*[@id='EscolaList']/a[1]")).click();
		}catch(NoSuchElementException | ElementNotVisibleException e){
			return null;
		}return new EscolaAdminForm();
	}
	
	public void clickDeleteButton(int index){
			escolaAdmin.findElement(By.xpath(
					"/html/body/div/div/section[2]/div[1]/table/thead[2]/tr["+(index+1)+"]/td[5]/form[2]")).click();
	}
	
	public EscolaAdminFormPage clickAcceptDeleteButton(){
		try{
			Alert javascriptAlert = driver.switchTo().alert();
		    System.out.println(javascriptAlert.getText()); // Get text on alert box
		    javascriptAlert.accept();
		}catch(NoSuchElementException | ElementNotVisibleException e){
			return null;
		}return new EscolaAdminFormPage();
		
	    
	}
	
}
