package page.admin;

import static org.junit.Assert.assertNotNull;

import java.util.NoSuchElementException;

import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.WebElement;

import build.AbstractTestCase;
import page.publico.LoginFormPage;

public class InstituicaoAdminFormPage extends AbstractTestCase{
	WebElement instituicaoAdmin;
	public InstituicaoAdminFormPage(){
		super();
		instituicaoAdmin = driver.findElement(By.xpath("/html/body/div"));
		assertNotNull("Não está na página de lista de Instituições", 
				instituicaoAdmin.findElement(By.id("InstituicaoList")));
	}
	
	public LoginFormPage clickLogoutButton(){
		try{
			instituicaoAdmin.findElement(By.xpath("/html/body/div/header/nav/div/ul/li/a")).click();
		}catch(NoSuchElementException | ElementNotVisibleException e){
			return null;
		}return new LoginFormPage();
	}
	
	public EscolaAdminFormPage clickEscolaListButton(){
		try{
			instituicaoAdmin.findElement(By.xpath("/html/body/div/aside/section/ul/li[1]/a/span")).click();
		}catch(NoSuchElementException | ElementNotVisibleException e){
			return null;
		}return new EscolaAdminFormPage();
	}
	
	public BairroAdminFormPage clickBairroListButton(){
		try{
			instituicaoAdmin.findElement(By.xpath("/html/body/div/aside/section/ul/li[2]/a/span")).click();
		}catch(NoSuchElementException | ElementNotVisibleException e){
			return null;
		}return new BairroAdminFormPage();
	}
	
	public InstituicaoAdminFormPage clickInstituicaoListButton(){
		try{
			instituicaoAdmin.findElement(By.xpath("/html/body/div/aside/section/ul/li[3]/a")).click();
		}catch(NoSuchElementException | ElementNotVisibleException e){
			return null;
		}return new InstituicaoAdminFormPage();
	}
	
	public VulnerabilidadeAdminFormPage clickVulnerabilidadeListButton(){
		try{
			instituicaoAdmin.findElement(By.xpath("/html/body/div/aside/section/ul/li[5]/a/span")).click();
		}catch(NoSuchElementException | ElementNotVisibleException e){
			return null;
		}return new VulnerabilidadeAdminFormPage();
	}
	
	public RequisicaoAdminFormPage clickRequisicaoListButton(){
		try{
			instituicaoAdmin.findElement(By.xpath("/html/body/div/aside/section/ul/li[6]/a/span")).click();
		}catch(NoSuchElementException | ElementNotVisibleException e){
			return null;
		}return new RequisicaoAdminFormPage();
	}
}
