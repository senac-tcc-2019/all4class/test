package page.admin;

import static org.junit.Assert.assertNotNull;

import java.util.NoSuchElementException;

import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.WebElement;

import build.AbstractTestCase;
import page.publico.LoginFormPage;

public class ResponsavelAdminFormPage extends AbstractTestCase{
	WebElement responsavelAdmin;
	public ResponsavelAdminFormPage(){
		super();
		responsavelAdmin = driver.findElement(By.xpath("/html/body/div"));
		assertNotNull("Não está na página de lista de responsável", 
				responsavelAdmin.findElement(By.id("ResponsavelList")));
	}
	
	public LoginFormPage clickLogoutButton(){
		try{
			responsavelAdmin.findElement(By.xpath("/html/body/div/header/nav/div/ul/li/a")).click();
		}catch(NoSuchElementException | ElementNotVisibleException e){
			return null;
		}return new LoginFormPage();
	}
	
	public EscolaAdminFormPage clickEscolaListButton(){
		try{
			responsavelAdmin.findElement(By.xpath("/html/body/div/aside/section/ul/li[1]/a/span")).click();
		}catch(NoSuchElementException | ElementNotVisibleException e){
			return null;
		}return new EscolaAdminFormPage();
	}
	
	public BairroAdminFormPage clickBairroListButton(){
		try{
			responsavelAdmin.findElement(By.xpath("/html/body/div/aside/section/ul/li[2]/a/span")).click();
		}catch(NoSuchElementException | ElementNotVisibleException e){
			return null;
		}return new BairroAdminFormPage();
	}
	
	public InstituicaoAdminFormPage clickInstituicaoListButton(){
		try{
			responsavelAdmin.findElement(By.xpath("/html/body/div/aside/section/ul/li[3]/a")).click();
		}catch(NoSuchElementException | ElementNotVisibleException e){
			return null;
		}return new InstituicaoAdminFormPage();
	}
	
	public VulnerabilidadeAdminFormPage clickVulnerabilidadeListButton(){
		try{
			responsavelAdmin.findElement(By.xpath("/html/body/div/aside/section/ul/li[5]/a/span")).click();
		}catch(NoSuchElementException | ElementNotVisibleException e){
			return null;
		}return new VulnerabilidadeAdminFormPage();
	}
	
	public RequisicaoAdminFormPage clickRequisicaoListButton(){
		try{
			responsavelAdmin.findElement(By.xpath("/html/body/div/aside/section/ul/li[6]/a/span")).click();
		}catch(NoSuchElementException | ElementNotVisibleException e){
			return null;
		}return new RequisicaoAdminFormPage();
	}
}
