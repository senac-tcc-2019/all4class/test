package page.admin;

import static org.junit.Assert.assertNotNull;

import java.util.NoSuchElementException;

import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.WebElement;

import build.AbstractTestCase;
import page.publico.LoginFormPage;

public class VulnerabilidadeAdminFormPage extends AbstractTestCase{
	WebElement vulnerabilidadeAdmin;
	public VulnerabilidadeAdminFormPage(){
		super();
		vulnerabilidadeAdmin = driver.findElement(By.xpath("/html/body/div"));
		assertNotNull("Não está na página de lista de vulnerabilidades", 
				vulnerabilidadeAdmin.findElement(By.id("VulnerabilidadeList")));
	}
	
	public LoginFormPage clickLogoutButton(){
		try{
			vulnerabilidadeAdmin.findElement(By.xpath("/html/body/div/header/nav/div/ul/li/a")).click();
		}catch(NoSuchElementException | ElementNotVisibleException e){
			return null;
		}return new LoginFormPage();
	}
	
	public EscolaAdminFormPage clickEscolaListButton(){
		try{
			vulnerabilidadeAdmin.findElement(By.xpath("/html/body/div/aside/section/ul/li[1]/a/span")).click();
		}catch(NoSuchElementException | ElementNotVisibleException e){
			return null;
		}return new EscolaAdminFormPage();
	}
	
	public BairroAdminFormPage clickBairroListButton(){
		try{
			vulnerabilidadeAdmin.findElement(By.xpath("/html/body/div/aside/section/ul/li[2]/a/span")).click();
		}catch(NoSuchElementException | ElementNotVisibleException e){
			return null;
		}return new BairroAdminFormPage();
	}
	
	public InstituicaoAdminFormPage clickInstituicaoListButton(){
		try{
			vulnerabilidadeAdmin.findElement(By.xpath("/html/body/div/aside/section/ul/li[3]/a")).click();
		}catch(NoSuchElementException | ElementNotVisibleException e){
			return null;
		}return new InstituicaoAdminFormPage();
	}
	
	public VulnerabilidadeAdminFormPage clickVulnerabilidadeListButton(){
		try{
			vulnerabilidadeAdmin.findElement(By.xpath("/html/body/div/aside/section/ul/li[5]/a/span")).click();
		}catch(NoSuchElementException | ElementNotVisibleException e){
			return null;
		}return new VulnerabilidadeAdminFormPage();
	}
	
	public RequisicaoAdminFormPage clickRequisicaoListButton(){
		try{
			vulnerabilidadeAdmin.findElement(By.xpath("/html/body/div/aside/section/ul/li[6]/a/span")).click();
		}catch(NoSuchElementException | ElementNotVisibleException e){
			return null;
		}return new RequisicaoAdminFormPage();
	}
}
