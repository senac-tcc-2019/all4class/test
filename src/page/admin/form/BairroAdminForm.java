package page.admin.form;

import static org.junit.Assert.assertNotNull;

import java.util.NoSuchElementException;

import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.WebElement;

import build.AbstractTestCase;
import page.admin.BairroAdminFormPage;

public class BairroAdminForm extends AbstractTestCase{
	
	WebElement bairroAdmin;
	public BairroAdminForm(){
		super();
		bairroAdmin = driver.findElement(By.xpath("/html/body/div"));
		assertNotNull("Não está na página de lista de bairros", bairroAdmin.findElement(By.id("BairroForm")));
	}
	
	public String getBairroNome(){
		String nome;
		try{
			nome = bairroAdmin.findElement(By.id("nome")).getAttribute("value");
		}catch(NoSuchElementException | ElementNotVisibleException e){
			return null;
		}return nome;
	}
	
	public BairroAdminFormPage clickVoltarButton(){
		try{
			bairroAdmin.findElement(By.xpath("/html/body/div/div/section[2]/div[2]/a")).click();
		}catch(NoSuchElementException | ElementNotVisibleException e){
			return null;
		}return new BairroAdminFormPage();
	}
}
