package page.admin.form;

import static org.junit.Assert.assertNotNull;

import java.util.NoSuchElementException;

import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import build.AbstractTestCase;
import entity.form.EscolaForm;
import page.admin.EscolaAdminFormPage;

public class EscolaAdminForm extends AbstractTestCase{
	WebElement escolaFormPage;
	public EscolaAdminForm(){
		super();
		escolaFormPage = driver.findElement(By.xpath("/html/body/div"));
		assertNotNull("Não está na página de form de escola", 
				escolaFormPage.findElement(By.id("EscolaForm")));
	}
	
	public String getEscolaFormNome(){
		String nome;
		try{
			nome = escolaFormPage.findElement(By.id("nome")).getAttribute("value");
		}catch(NoSuchElementException | ElementNotVisibleException e){
			return null;
		}return nome;
	}
	
	public String getEscolaFormBairro(){
		String bairro;
		try{
			Select select = new Select(escolaFormPage.findElement(By.xpath("//*[@id='bairro']")));
			WebElement option = select.getFirstSelectedOption();
			bairro = option.getText();
			
		}catch(NoSuchElementException | ElementNotVisibleException e){
			return null;
		}return bairro;
	}
	
	public String getEscolaFormEndereco(){
		String endereco;
		try{
			endereco = escolaFormPage.findElement(By.id("endereco")).getAttribute("value");
		}catch(NoSuchElementException | ElementNotVisibleException e){
			return null;
		}return endereco;
	}
	
	public String getEscolaFormResponsavel(){
		String responsavel;
		try{
			Select select = new Select(escolaFormPage.findElement(By.xpath("//*[@id='responsavel']")));
			WebElement option = select.getFirstSelectedOption();
			responsavel = option.getText();
		}catch(NoSuchElementException | ElementNotVisibleException e){
			return null;
		}return responsavel;
	}
	
	public EscolaForm getEscolaFormInformation(){
		EscolaForm escolaForm = new EscolaForm();
		escolaForm.addEscola(getEscolaFormNome(), getEscolaFormBairro(), getEscolaFormEndereco(), getEscolaFormResponsavel());
		return escolaForm;
	}
	
	public EscolaAdminForm setEscolaNome(String nome){
		try{
			escolaFormPage.findElement(By.xpath("//*[@id='nome']")).sendKeys(nome);
		}catch(NoSuchElementException | ElementNotVisibleException e){
			return null;
		}return new EscolaAdminForm();
	}
	
	public EscolaAdminForm setEscolaBairro(int index){
		try{
			Select select = new Select(escolaFormPage.findElement(By.xpath("//*[@id='bairro']")));
			select.selectByIndex(index);
		}catch(NoSuchElementException | ElementNotVisibleException e){
			return null;
		}return new EscolaAdminForm();
	}
	
	public EscolaAdminForm setEscolaResponsavel(int index){
		try{
			Select select = new Select(escolaFormPage.findElement(By.xpath("//*[@id='responsavel']")));
			select.selectByIndex(index);
		}catch(NoSuchElementException | ElementNotVisibleException e){
			return null;
		}return new EscolaAdminForm();
	}
	
	public EscolaAdminForm setEscolaEndereco(String endereco){
		try{
			escolaFormPage.findElement(By.xpath("//*[@id='endereco']")).sendKeys(endereco);
		}catch(NoSuchElementException | ElementNotVisibleException e){
			return null;
		}return new EscolaAdminForm();
	}
	
	public EscolaAdminFormPage clickSalvarButton(){
		try{
			escolaFormPage.findElement(By.xpath("/html/body/div/div/section[2]/form/input[2]")).click();
		}catch(NoSuchElementException | ElementNotVisibleException e){
			return null;
		}return new EscolaAdminFormPage();
	}
}
