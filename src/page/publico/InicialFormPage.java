package page.publico;

import static org.junit.Assert.assertNotNull;

import java.util.NoSuchElementException;

import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.WebElement;

import build.AbstractTestCase;
import util.Util;

public class InicialFormPage extends AbstractTestCase{
	WebElement inicio;
	By mapaButton = By.xpath("//*[@id='paginaMapa']/a");
	By participeButton = By.xpath("//*[@id='paginaParticipe']/a");
	By loginButton = By.xpath("//*[@id='paginaLogin']/a");
	public InicialFormPage(){
		super();
		inicio = driver.findElement(By.id("inicio"));
		assertNotNull("Não está na página inicial", inicio);
	}
	
	public MapaFormPage goToMapaPage(){
		try{
			inicio.findElement(mapaButton).click();
		}catch(NoSuchElementException | ElementNotVisibleException e){
			return null;
		}return new MapaFormPage();
	}
	
	public ParticipeFormPage goToParticipePage(){
		try{
			inicio.findElement(participeButton).click();
		}catch(NoSuchElementException | ElementNotVisibleException e){
			return null;
		}return new ParticipeFormPage();
	}
	
	public LoginFormPage goToLoginPage(){
		try{
			inicio.findElement(loginButton).click();
		}catch(NoSuchElementException | ElementNotVisibleException e){
			return null;
		}return new LoginFormPage();
	}
	

}
