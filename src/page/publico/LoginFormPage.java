package page.publico;

import static org.junit.Assert.assertNotNull;

import java.util.NoSuchElementException;

import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.WebElement;

import build.AbstractTestCase;
import page.admin.HomeAdminFormPage;

public class LoginFormPage extends AbstractTestCase{
	WebElement loginPage;
	
	public LoginFormPage(){
		super();
		loginPage = driver.findElement(By.id("loginPage"));
		assertNotNull("Você não está na página de login.", loginPage);
	}
	
	public LoginFormPage setLogin(String login){
		try{
			loginPage.findElement(By.id("email")).sendKeys(login);
		}catch(NoSuchElementException | ElementNotVisibleException e){
			return null;
		}return new LoginFormPage();
		
	}
	
	public LoginFormPage setSenha(String senha){
		try{
			loginPage.findElement(By.id("password")).sendKeys(senha);
		}catch(NoSuchElementException | ElementNotVisibleException e){
			return null;
		}return new LoginFormPage();
	}
	
	public LoginFormPage checkRememberMe(){
		try{
			loginPage.findElement(By.id("remember")).click();
		}catch(NoSuchElementException | ElementNotVisibleException e){
			return null;
		}return new LoginFormPage();
	}
	
	public HomeAdminFormPage clickLogarButton(){
		try{
			loginPage.findElement(
					By.xpath("//*[@id='loginPage']/div/div/div/div[2]/form/div[4]/div/button")).click();
		}catch(NoSuchElementException | ElementNotVisibleException e){
			return null;
		}return new HomeAdminFormPage();
	}
	
	
}
