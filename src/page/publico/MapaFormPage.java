package page.publico;

import static org.junit.Assert.assertNotNull;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import build.AbstractTestCase;

public class MapaFormPage extends AbstractTestCase{
	WebElement mapa;
	
	public MapaFormPage(){
		super();
		mapa = driver.findElement(By.id("mapa"));
		assertNotNull("Não está na página de Mapa", mapa);
	}
}
