package page.publico;

import static org.junit.Assert.assertNotNull;

import java.util.NoSuchElementException;

import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.WebElement;

import build.AbstractTestCase;

public class ParticipeFormPage extends AbstractTestCase{

	WebElement participe;
	By mapaButton = By.xpath("//*[@id='paginaMapa']/a");
	By participeButton = By.xpath("//*[@id='paginaParticipe']/a");
	By loginButton = By.xpath("//*[@id='paginaLogin']/a");
	By inicioButton = By.xpath("//*[@id='paginaInicial']/a");
	By nomeField = By.xpath("/html/body/div/form/input[2]");
	By telefoneField = By.xpath("/html/body/div/form/input[3]");
	By emailField = By.xpath("/html/body/div/form/input[4]");
	public ParticipeFormPage(){
		super();
		participe = driver.findElement(By.id("participe"));
		assertNotNull("Não está na página de participe", participe);
	}
	
	public MapaFormPage goToMapaPage(){
		try{
			participe.findElement(mapaButton).click();
		}catch(NoSuchElementException | ElementNotVisibleException e){
			return null;
		}return new MapaFormPage();
	}
	
	public InicialFormPage goToParticipePage(){
		try{
			participe.findElement(inicioButton).click();
		}catch(NoSuchElementException | ElementNotVisibleException e){
			return null;
		}return new InicialFormPage();
	}
	
	public LoginFormPage goToLoginPage(){
		try{
			participe.findElement(loginButton).click();
		}catch(NoSuchElementException | ElementNotVisibleException e){
			return null;
		}return new LoginFormPage();
	}
	
	public ParticipeFormPage setNome(String nome){
		try{
			participe.findElement(nomeField).sendKeys(nome);
		}catch(NoSuchElementException | ElementNotVisibleException e){
			return null;
		}return new ParticipeFormPage();
	}
	
	public String getNome(){
		String nome="";
		try{
			nome = participe.findElement(nomeField).getAttribute("value");
		}catch(NoSuchElementException | ElementNotVisibleException e){
			return null;
		}return nome;
	}
	
	public ParticipeFormPage setTelefone(String telefone){
		try{
			participe.findElement(telefoneField).sendKeys(telefone);
		}catch(NoSuchElementException | ElementNotVisibleException e){
			return null;
		}return new ParticipeFormPage();
	}
	
	public String getTelefone(){
		String telefone="";
		try{
			telefone = participe.findElement(telefoneField).getAttribute("value");
		}catch(NoSuchElementException | ElementNotVisibleException e){
			return null;
		}return telefone;
	}
	
	public ParticipeFormPage setEmail(String email){
		try{
			participe.findElement(emailField).sendKeys(email);
		}catch(NoSuchElementException | ElementNotVisibleException e){
			return null;
		}return new ParticipeFormPage();
	}
	
	public String getEmail(){
		String email="";
		try{
			email = participe.findElement(emailField).getAttribute("value");
		}catch(NoSuchElementException | ElementNotVisibleException e){
			return null;
		}return email;
	}
	
	public ParticipeFormPage selectEscola (int index){
		try{
			participe.findElement(By.xpath("/html/body/div/form/select[1]")).click();
			participe.findElement(By.xpath("/html/body/div/form/select[1]/option["+index+"]")).click();
		}catch(NoSuchElementException | ElementNotVisibleException e){
			return null;
		}return new ParticipeFormPage();
	}
	
	public ParticipeFormPage selectInstituicao (int index){
		try{
			participe.findElement(By.xpath("/html/body/div/form/select[2]")).click();
			participe.findElement(By.xpath("/html/body/div/form/select[2]/option["+index+"]")).click();
		}catch(NoSuchElementException | ElementNotVisibleException e){
			return null;
		}return new ParticipeFormPage();
	}
	
	public ParticipeFormPage selectVulnerabilidade (int index){
		try{
			participe.findElement(By.xpath("/html/body/div/form/select[3]")).click();
			participe.findElement(By.xpath("/html/body/div/form/select[3]/option["+index+"]")).click();
		}catch(NoSuchElementException | ElementNotVisibleException e){
			return null;
		}return new ParticipeFormPage();
	}
	
	public ParticipeFormPage setDescricao(String descricao){
		try{
			participe.findElement(By.xpath("/html/body/div/form/textarea")).sendKeys(descricao);
		}catch(NoSuchElementException | ElementNotVisibleException e){
			return null;
		}return new ParticipeFormPage();
	}
	
	public MapaFormPage clickEnviar(){
		try{
			participe.findElement(By.xpath("/html/body/div/form/input[5]")).click();
		}catch(NoSuchElementException | ElementNotVisibleException e){
			return null;
		}return new MapaFormPage();
	}
	
	
	
	
}
