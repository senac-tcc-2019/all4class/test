package runners;

import java.io.File;
import java.util.HashMap;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.phantomjs.PhantomJSDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriverService;
import org.openqa.selenium.remote.DesiredCapabilities;

public class DriverCreator {

	public WebDriver chromeDriver() {
		WebDriver driver;

		DesiredCapabilities cap = DesiredCapabilities.chrome();
		cap.setBrowserName("Chrome browser");

		new File("automateTesting/downloads").mkdirs();
		File folder = new File("automateTesting/downloads");

		HashMap<String, Object> chromePrefs = new HashMap<String, Object>();
		chromePrefs.put("profile.default_content_settings.popups", 0);
		chromePrefs.put("download.default_directory", folder.getAbsolutePath());

		// System.setProperty("webdriver.chrome.logfile",
		// folder.getAbsolutePath());

		ChromeOptions options = new ChromeOptions();
		options.setExperimentalOption("prefs", chromePrefs);
		cap.setCapability(ChromeOptions.CAPABILITY, options);

		System.setProperty("webdriver.chrome.driver", "src/chromedriver.exe");
		// cap.setCapability(ChromeDriverService.CHROME_DRIVER_EXE_PROPERTY,
		// "chromedriver.exe");
		try {
			driver = new ChromeDriver(cap);

			driver.manage().window().maximize();

		} catch (Exception e) {
			e.printStackTrace();
			driver = null;
		}

		return driver;

		// WebDriver driver = new ChromeDriver();
		// return driver;*/
	}

	public WebDriver chromeDriverLinux() {
		WebDriver driver;

		DesiredCapabilities cap = DesiredCapabilities.chrome();
		cap.setBrowserName("Chrome browser");

		new File("automateTesting/downloads").mkdirs();
		File folder = new File("automateTesting/downloads");

		HashMap<String, Object> chromePrefs = new HashMap<String, Object>();
		chromePrefs.put("profile.default_content_settings.popups", 0);
		chromePrefs.put("download.default_directory", folder.getAbsolutePath());

		// System.setProperty("webdriver.chrome.logfile",
		// folder.getAbsolutePath());

		ChromeOptions options = new ChromeOptions();
		options.setExperimentalOption("prefs", chromePrefs);
		cap.setCapability(ChromeOptions.CAPABILITY, options);

		System.setProperty("webdriver.chrome.driver", "automateTesting/chromedriver");
		// cap.setCapability(ChromeDriverService.CHROME_DRIVER_EXE_PROPERTY,
		// "chromedriver.exe");
		try {
			driver = new ChromeDriver(cap);

			driver.manage().window().maximize();

		} catch (Exception e) {
			e.printStackTrace();
			driver = null;
		}

		return driver;

		// WebDriver driver = new ChromeDriver();
		// return driver;*/
	}

	public WebDriver phantomJsDriver() {
		WebDriver driver;

		DesiredCapabilities cap = new DesiredCapabilities();
		cap.setBrowserName("Phantom browser");
		cap.setCapability(PhantomJSDriverService.PHANTOMJS_EXECUTABLE_PATH_PROPERTY, "phantomjs-1.9.8-windows/phantomjs.exe");
		try {
			driver = new PhantomJSDriver(cap);

			System.out.println(driver.getCurrentUrl());
			System.out.println(driver.getTitle());
		} catch (Exception e) {
			e.printStackTrace();
			driver = null;
		}

		return driver;
	}

	public WebDriver firefoxDriver() {
		WebDriver driver;
		FirefoxProfile firefoxProfile;
		try {
			firefoxProfile = new FirefoxProfile();
			// File prof = new File("automateTesting/k3b7du0t.default");
			// firefoxProfile = new FirefoxProfile(prof);// application/Portable
			// Document Format
			// application/Adobe
			// Acrobat Document
			// //C:/Users/lluz/AppData/Roaming/Mozilla/Firefox/Profiles/k3b7du0t.default
			// firefoxProfile.addExtension(new
			// File("k3b7du02t.default/extensions/keyconfig.xpi"));

		} catch (Exception e) {
			e.printStackTrace();
			firefoxProfile = new FirefoxProfile();

		}
		// Sets the pdf and xls files to be saved automatically.
		firefoxProfile.setPreference("browser.helperApps.neverAsk.saveToDisk",
				"application/xlsx,application/xls,application/excell,application/octet-stream,application/pdf,application/Portable Document Format");

		// Used for the download .xls files test. Sets the default download
		// folder to be inside the project.
		new File("automateTesting/downloads").mkdirs();
		File folder = new File("automateTesting/downloads");
		firefoxProfile.setPreference("browser.download.dir", folder.getAbsolutePath());
		firefoxProfile.setPreference("browser.download.folderList", 2);

		System.setProperty("webdriver.gecko.driver", "automateTesting/geckodriver.exe");

		driver = new FirefoxDriver();
		

		return driver;
	}
}
