package suite.escola;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import test.addEscolaValidaDadosTest;
import test.escolaDadosTest;
import test.escolaValidaDelete;
import test.escolaValidaFiltroNome;
import tests.suite.SuitesAbstractTestCase;



@RunWith(Suite.class)
@SuiteClasses({ addEscolaValidaDadosTest.class, escolaDadosTest.class, escolaValidaDelete.class, 
		escolaValidaFiltroNome.class })
	public class EscolaAllTests extends SuitesAbstractTestCase {
		@BeforeClass
		public static void setUpa() {
			/*
			 * generalTests = true; if (driver != null) { if (urls == null &&
			 * needUrls) { urls = runner.getAllTemplateURLs(); } }
			 */
		}

		@AfterClass
		public static void close() {
			driver.close();
			driver.quit();
		}
}
