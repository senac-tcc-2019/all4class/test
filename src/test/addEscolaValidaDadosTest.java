package test;
import build.AbstractTestCase;
import entity.form.EscolaForm;
import page.admin.BairroAdminFormPage;
import page.admin.EscolaAdminFormPage;
import page.admin.HomeAdminFormPage;
import page.admin.InstituicaoAdminFormPage;
import page.admin.VulnerabilidadeAdminFormPage;
import page.admin.form.EscolaAdminForm;
import page.publico.InicialFormPage;
import page.publico.LoginFormPage;
import page.publico.MapaFormPage;
import page.publico.ParticipeFormPage;
import util.Util;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assume.assumeTrue;
import static util.Assertions.assertEquals;
import static util.Assertions.assertNotNull;
import static util.Assertions.assertThat;

import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.JavascriptExecutor;
public class addEscolaValidaDadosTest extends AbstractTestCase{
	public static InicialFormPage inicio;
	public static MapaFormPage mapa;
	public static ParticipeFormPage participe;
	public static LoginFormPage loginPage;
	public static HomeAdminFormPage homeAdmin;
	public static EscolaAdminFormPage escolaAdmin;
	public static BairroAdminFormPage bairroAdmin;
	public static InstituicaoAdminFormPage instituicaoAdmin;
	public static VulnerabilidadeAdminFormPage vulnerabilidadeAdmin;
	public static EscolaForm escolaForm;
	public static EscolaForm escolaFormEdit;
	public static EscolaAdminForm escolaAdminForm;
	
	@BeforeClass
	public static void addEscola(){
		homeAdmin = new HomeAdminFormPage();
		Util.wait(5000);
		assertNotNull("Não achei o botão de escola list", escolaAdmin = homeAdmin.clickEscolaListButton());
		Util.wait(4000);
		assertNotNull("Não foi possível clicar no botão de adicionar escola", 
				escolaAdminForm = escolaAdmin.clickAddEscolaButton());
		Util.wait(4000);
		assertNotNull("Não foi possível setar o nome da escola", escolaAdminForm.setEscolaNome("Selenium"+Math.random()));
		Util.wait(4000);
		assertNotNull("Não foi possível setar o endereço da escola", escolaAdminForm.setEscolaEndereco("Selenium"+Math.random()));
		Util.wait(4000);
		assertNotNull("Não foi possível setar um bairro para a escola", escolaAdminForm.setEscolaBairro(1));
		Util.wait(4000);
		assertNotNull("Não foi possível setar um responsável para a escola", escolaAdminForm.setEscolaResponsavel(1));
		Util.wait(4000);
		escolaFormEdit = new EscolaForm();
		escolaFormEdit = escolaAdminForm.getEscolaFormInformation();
		Util.wait(4000);
		assertNotNull("Não foi possível clicar no botão de salvar escola", escolaAdmin = escolaAdminForm.clickSalvarButton());
		Util.wait(4000);
		assertNotNull("Não foi possível setar filtro de nome para escola", escolaAdmin.setNomeFiltro(escolaFormEdit.getEscolaNome(0).trim()));
		Util.wait(4000);
		assertNotNull("Não foi possível clicar no botão de filtrar", escolaAdmin = escolaAdmin.clickFiltrarButton());
		Util.wait(4000);
		escolaForm = new EscolaForm();
		escolaForm = escolaAdmin.getEscolaListInfo();
		Util.wait(4000);
		
		
	}
	
	@Test
	public void testNomeEscola(){
		assertThat(escolaForm.getEscolaNome(0).trim(), equalTo(escolaFormEdit.getEscolaNome(0).trim()));
	}
	
	@Test
	public void testBairroEscola(){
		assertThat(escolaForm.getEscolaBairro(0).trim(), equalTo(escolaFormEdit.getEscolaBairro(0).trim()));
	}
	
	@Test
	public void testEnderecoEscola(){
		assertThat(escolaForm.getEscolaEndereco(0).trim(), equalTo(escolaFormEdit.getEscolaEndereco(0).trim()));
	}
}
