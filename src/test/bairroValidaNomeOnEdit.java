package test;

import entity.form.BairroForm;
import entity.form.EscolaForm;
import page.admin.BairroAdminFormPage;
import page.admin.EscolaAdminFormPage;
import page.admin.HomeAdminFormPage;
import page.admin.InstituicaoAdminFormPage;
import page.admin.VulnerabilidadeAdminFormPage;
import page.admin.form.BairroAdminForm;
import page.admin.form.EscolaAdminForm;
import page.publico.InicialFormPage;
import page.publico.LoginFormPage;
import page.publico.MapaFormPage;
import page.publico.ParticipeFormPage;
import util.Util;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assume.assumeTrue;
import static util.Assertions.assertEquals;
import static util.Assertions.assertNotNull;
import static util.Assertions.assertThat;

import java.util.ArrayList;

import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.JavascriptExecutor;
import build.AbstractTestCase;

public class bairroValidaNomeOnEdit extends AbstractTestCase{
	public static InicialFormPage inicio;
	public static MapaFormPage mapa;
	public static ParticipeFormPage participe;
	public static LoginFormPage loginPage;
	public static HomeAdminFormPage homeAdmin;
	public static EscolaAdminFormPage escolaAdmin;
	public static BairroAdminFormPage bairroAdmin;
	public static InstituicaoAdminFormPage instituicaoAdmin;
	public static VulnerabilidadeAdminFormPage vulnerabilidadeAdmin;
	public static BairroAdminForm bairroForm;
	public static ArrayList<String> bairroNome;
	public static ArrayList<String> bairroNomeEdit;
	
	@BeforeClass
	public static void setUp(){
		homeAdmin = new HomeAdminFormPage();
		Util.wait(3000);
		assertNotNull("Botão Bairros no AdmonLTE não encontrado", bairroAdmin = homeAdmin.clickBairroListButton());
		Util.wait(4000);
		bairroNome = new ArrayList<String>();
		bairroNomeEdit = new ArrayList<String>();
		for (int i = 0; i < bairroAdmin.getNumberOfBairros(); i++){
			bairroNome.add(bairroAdmin.getBairroNome(i));
			Util.wait(2000);
			assertNotNull("Botão de edit não foi possível de clicar", bairroForm = bairroAdmin.clickEditButton(i));
			Util.wait(3000);
			bairroNomeEdit.add(bairroForm.getBairroNome());
			Util.wait(3000);
			bairroAdmin = bairroForm.clickVoltarButton();
			Util.wait(3000);
		}
		
	}
	
	@Test
	public void testNomeBairrosOnEdit(){
		for (int i = 0; i < bairroNome.size(); i++){
			assertThat(bairroNome.get(i), equalTo(bairroNomeEdit.get(i)));
			System.out.println("bairro list: "+bairroNome.get(i));
			System.out.println("bairro form: "+bairroNomeEdit.get(i));
		}
	}
	
}
