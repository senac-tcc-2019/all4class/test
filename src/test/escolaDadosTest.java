package test;

import build.AbstractTestCase;
import entity.form.EscolaForm;
import page.admin.BairroAdminFormPage;
import page.admin.EscolaAdminFormPage;
import page.admin.HomeAdminFormPage;
import page.admin.InstituicaoAdminFormPage;
import page.admin.VulnerabilidadeAdminFormPage;
import page.admin.form.EscolaAdminForm;
import page.publico.InicialFormPage;
import page.publico.LoginFormPage;
import page.publico.MapaFormPage;
import page.publico.ParticipeFormPage;
import util.Util;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assume.assumeTrue;
import static util.Assertions.assertEquals;
import static util.Assertions.assertNotNull;
import static util.Assertions.assertThat;
import org.junit.Test;
import org.openqa.selenium.JavascriptExecutor;
public class escolaDadosTest extends AbstractTestCase{
	public InicialFormPage inicio;
	public MapaFormPage mapa;
	public ParticipeFormPage participe;
	public LoginFormPage loginPage;
	public HomeAdminFormPage homeAdmin;
	public EscolaAdminFormPage escolaAdmin;
	public BairroAdminFormPage bairroAdmin;
	public InstituicaoAdminFormPage instituicaoAdmin;
	public VulnerabilidadeAdminFormPage vulnerabilidadeAdmin;
	public EscolaForm escolaForm;
	public EscolaForm escolaFormEdit;
	public EscolaAdminForm escolaAdminForm;
	
	@Test
	public void verificaDadosNoEdit(){
		homeAdmin = new HomeAdminFormPage();
		assertNotNull("Não achei o botão de escola list", escolaAdmin = homeAdmin.clickEscolaListButton());
		Util.wait(4000);
		escolaForm = new EscolaForm();
		escolaForm = escolaAdmin.getEscolaListInfo();
		Util.wait(3000);
		assertNotNull("Não foi possível clicar no botão de edit.", escolaAdminForm = escolaAdmin.clickEditButton(0));
		assertNotNull("Não foi direcionado à página de edit de escola.", escolaAdminForm);
		escolaFormEdit = new EscolaForm();
		escolaFormEdit = escolaAdminForm.getEscolaFormInformation();
		Util.wait(3000);
		
		assertThat(escolaForm.getEscolaNome(0).trim(), equalTo(escolaFormEdit.getEscolaNome(0).trim()));
		assertThat(escolaForm.getEscolaBairro(0).trim(), equalTo(escolaFormEdit.getEscolaBairro(0).trim()));
		assertThat(escolaForm.getEscolaEndereco(0), equalTo(escolaFormEdit.getEscolaEndereco(0).trim()));
		
		
		
	}
}
