package test;
import entity.form.EscolaForm;
import page.admin.BairroAdminFormPage;
import page.admin.EscolaAdminFormPage;
import page.admin.HomeAdminFormPage;
import page.admin.InstituicaoAdminFormPage;
import page.admin.RequisicaoAdminFormPage;
import page.admin.VulnerabilidadeAdminFormPage;
import page.admin.form.EscolaAdminForm;
import page.publico.InicialFormPage;
import page.publico.LoginFormPage;
import page.publico.MapaFormPage;
import page.publico.ParticipeFormPage;
import util.Util;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assume.assumeTrue;
import static util.Assertions.assertEquals;
import static util.Assertions.assertNotNull;
import static util.Assertions.assertThat;

import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import build.AbstractTestCase;

public class escolaValidaDelete extends AbstractTestCase{
	public static InicialFormPage inicio;
	public static MapaFormPage mapa;
	public static ParticipeFormPage participe;
	public static LoginFormPage loginPage;
	public static HomeAdminFormPage homeAdmin;
	public static EscolaAdminFormPage escolaAdmin;
	public static BairroAdminFormPage bairroAdmin;
	public static InstituicaoAdminFormPage instituicaoAdmin;
	public static VulnerabilidadeAdminFormPage vulnerabilidadeAdmin;
	public static RequisicaoAdminFormPage requisicaoAdmin;
	public static String escolaNome;
	public static EscolaAdminForm escolaAdminForm;
	public static EscolaForm escolaFormEdit;
	
	
	@Test
	public void testRecusaExclusao(){
		homeAdmin = new HomeAdminFormPage();
		assertNotNull("Não achei o botão de requisição", requisicaoAdmin = homeAdmin.clickRequisicaoListButton());
		Util.wait(5000);
		assertNotNull("Não foi possível achar o nome da escola na requisição", escolaNome = requisicaoAdmin.getEscolaNome(0));
		Util.wait(3000);
		assertNotNull("Não achei o botão pra lista de escolas", escolaAdmin = requisicaoAdmin.clickEscolaListButton());
		Util.wait(3000);
		assertNotNull("Não foi possível preencher o filtro de nome de escola", 
				escolaAdmin.setNomeFiltro(escolaNome));
		Util.wait(3000);
		assertNotNull("Não foi possível clicar no botão de filtrar", escolaAdmin = escolaAdmin.clickFiltrarButton());
		Util.wait(3000);
		assertThat(escolaNome, equalTo(escolaAdmin.getEscolaNome(0)));
		Util.wait(3000);
		escolaAdmin.clickDeleteButton(0);
		Util.wait(3000);
		assertNotNull("Não foi possível confirmar a esclusão de escola", escolaAdmin = escolaAdmin.clickAcceptDeleteButton());
		Util.wait(1000);
		assertThat(driver.findElement(By.xpath("/html/body/div/div/section[2]/div[1]")).getText(), containsString("vinculadas"));
	
	}
		
		@Test
		public void testAceitaExclusao(){
			Util.wait(4000);
			assertNotNull("Não foi possível clicar no botão de adicionar escola", 
					escolaAdminForm = escolaAdmin.clickAddEscolaButton());
			Util.wait(4000);
			assertNotNull("Não foi possível setar o nome da escola", escolaAdminForm.setEscolaNome("Selenium"+Math.random()));
			Util.wait(4000);
			assertNotNull("Não foi possível setar o endereço da escola", escolaAdminForm.setEscolaEndereco("Selenium"+Math.random()));
			Util.wait(4000);
			assertNotNull("Não foi possível setar um bairro para a escola", escolaAdminForm.setEscolaBairro(1));
			Util.wait(4000);
			assertNotNull("Não foi possível setar um responsável para a escola", escolaAdminForm.setEscolaResponsavel(1));
			Util.wait(4000);
			escolaFormEdit = new EscolaForm();
			escolaFormEdit = escolaAdminForm.getEscolaFormInformation();
			Util.wait(4000);
			assertNotNull("Não foi possível clicar no botão de salvar escola", escolaAdmin = escolaAdminForm.clickSalvarButton());
			Util.wait(4000);
			assertNotNull("Não foi possível preencher o filtro de nome de escola", 
					escolaAdmin.setNomeFiltro(escolaFormEdit.getEscolaNome(0).trim()));
			Util.wait(3000);
			assertNotNull("Não foi possível clicar no botão de filtrar", escolaAdmin = escolaAdmin.clickFiltrarButton());
			Util.wait(3000);
			assertThat(escolaFormEdit.getEscolaNome(0).trim(), equalTo(escolaAdmin.getEscolaNome(0)));
			Util.wait(3000);
			escolaAdmin.clickDeleteButton(0);
			Util.wait(3000);
			assertNotNull("Não foi possível confirmar a esclusão de escola", escolaAdmin = escolaAdmin.clickAcceptDeleteButton());
			Util.wait(1000);
			Util.wait(1000);
			System.out.println("alert: "+driver.findElement(By.xpath("/html/body/div/div/section[2]/div[1]")).getText());
			assertThat(driver.findElement(By.xpath("/html/body/div/div/section[2]/div[1]")).getText(), containsString("excluída"));
			
			
		}
		
		
}
