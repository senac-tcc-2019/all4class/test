package test;

import build.AbstractTestCase;
import build.AbstractTestCase;
import entity.form.EscolaForm;
import page.admin.BairroAdminFormPage;
import page.admin.EscolaAdminFormPage;
import page.admin.HomeAdminFormPage;
import page.admin.InstituicaoAdminFormPage;
import page.admin.VulnerabilidadeAdminFormPage;
import page.admin.form.EscolaAdminForm;
import page.publico.InicialFormPage;
import page.publico.LoginFormPage;
import page.publico.MapaFormPage;
import page.publico.ParticipeFormPage;
import util.Util;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.*;
import static org.junit.Assume.assumeTrue;
import static util.Assertions.assertEquals;
import static util.Assertions.assertNotNull;
import static util.Assertions.assertThat;

import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.JavascriptExecutor;
public class escolaValidaFiltroNome extends AbstractTestCase{
	public static InicialFormPage inicio;
	public static MapaFormPage mapa;
	public static ParticipeFormPage participe;
	public static LoginFormPage loginPage;
	public static HomeAdminFormPage homeAdmin;
	public static EscolaAdminFormPage escolaAdmin;
	public static BairroAdminFormPage bairroAdmin;
	public static InstituicaoAdminFormPage instituicaoAdmin;
	public static VulnerabilidadeAdminFormPage vulnerabilidadeAdmin;
	public static EscolaForm escolaForm;
	public static EscolaForm escolaFormEdit;
	public static EscolaAdminForm escolaAdminForm;
	public static String nome;
	public static String nome2;
	
	@BeforeClass
	public static void setFilter(){
		homeAdmin = new HomeAdminFormPage();
		assertNotNull("Não achei o botão de escola list", escolaAdmin = homeAdmin.clickEscolaListButton());
		Util.wait(4000);
		
		assertNotNull("Não foi possível pegar o nome da primeira escola da lista", nome = escolaAdmin.getEscolaNome(0));
		Util.wait(4000);
		assertNotNull("Não foi possível setar valor no filtro de nome", escolaAdmin.setNomeFiltro(nome));
		Util.wait(4000);
		assertNotNull("Não foi possível clicar no botão de filtrar", escolaAdmin = escolaAdmin.clickFiltrarButton());
		Util.wait(4000);
		assertNotNull("Não foi possível pegar o nome da primeira escola da lista após filtrar", nome2 = escolaAdmin.getEscolaNome(0));
		Util.wait(4000);
		
	}
	
	@Test
	public void testaNomeFiltro(){
		assertTrue(nome.trim().equals(nome2.trim()));
	}
}
