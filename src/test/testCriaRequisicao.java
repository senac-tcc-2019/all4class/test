package test;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assume.assumeTrue;
import static util.Assertions.assertEquals;
import static util.Assertions.assertNotNull;
import static util.Assertions.assertThat;
import org.junit.Test;
import org.openqa.selenium.JavascriptExecutor;

import build.AbstractTestCase;
import entity.form.EscolaForm;
import page.admin.BairroAdminFormPage;
import page.admin.EscolaAdminFormPage;
import page.admin.HomeAdminFormPage;
import page.admin.InstituicaoAdminFormPage;
import page.admin.VulnerabilidadeAdminFormPage;
import page.admin.form.EscolaAdminForm;
import page.publico.InicialFormPage;
import page.publico.LoginFormPage;
import page.publico.MapaFormPage;
import page.publico.ParticipeFormPage;
import util.Util;

public class testCriaRequisicao extends AbstractTestCase{

	public InicialFormPage inicio;
	public MapaFormPage mapa;
	public ParticipeFormPage participe;
	public LoginFormPage loginPage;
	public HomeAdminFormPage homeAdmin;
	public EscolaAdminFormPage escolaAdmin;
	public BairroAdminFormPage bairroAdmin;
	public InstituicaoAdminFormPage instituicaoAdmin;
	public VulnerabilidadeAdminFormPage vulnerabilidadeAdmin;
	public EscolaForm escolaForm;
	public EscolaForm escolaFormEdit;
	public EscolaAdminForm escolaAdminForm;
	
	@Test
	public void criarRequisicaoTest(){
		AbstractTestCase.driver.get("http://127.0.0.1:8000/");
		try {
			Thread.sleep(4000);
		} catch (InterruptedException e) {

			e.printStackTrace();
		}
		inicio = new InicialFormPage();
		participe = inicio.goToParticipePage();
		assertNotNull("nome", participe.setNome("Selenium"+Math.random()));
		Util.wait(4000);
		assertNotNull("telefone", participe.setTelefone("(53)991996633"));
		Util.wait(4000);
		assertNotNull("email", participe.setEmail("SeleniumTest@gmail.com"));
		Util.wait(4000);
		assertNotNull("escola", participe.selectEscola(2));
		Util.wait(4000);
		assertNotNull("instituição", participe.selectInstituicao(2));
		Util.wait(4000);
		assertNotNull("vulnerabilidade", participe.selectVulnerabilidade(2));
		Util.wait(4000);
		assertNotNull("descrição", participe.setDescricao("Selenium"+Math.random()));
		Util.wait(4000);
		assertNotNull("enviar", mapa = participe.clickEnviar());
		Util.wait(4000);
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("window.scrollBy(0,1000)");
		Util.wait(4000);
		
		
	}
	
	
}
