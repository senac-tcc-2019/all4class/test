package util;

import static build.AbstractTestCase.driver;

import static build.AbstractTestCase.properties;


import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import build.AbstractTestCase;
import runners.DriverCreator;
import util.Util;



import org.apache.commons.io.FileUtils;
import org.openqa.selenium.*;


public class Util {

	public static void wait(int milisecond) {
		try {
			Thread.sleep(milisecond);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static WebDriver initialize(String url) {
		DriverCreator driverCreator = new DriverCreator();
		//DriverCreator driverCreator = new DriverCreator();
		
		String browserName = AbstractTestCase.properties.getProperty("browser");
		if (Util.OSDetector().contains("Linux")) {
			if (AbstractTestCase.properties.getProperty("browser").equalsIgnoreCase("chrome")) {
				driver = driverCreator.chromeDriverLinux();
			}

		} else {
			if (browserName.equalsIgnoreCase("firefox")) {
				driver = driverCreator.firefoxDriver();
			}
			if (AbstractTestCase.properties.getProperty("browser").equalsIgnoreCase("phantomjs")) {
				driver = driverCreator.phantomJsDriver();
			}
			if (AbstractTestCase.properties.getProperty("browser").equalsIgnoreCase("chrome")) {
				driver = driverCreator.chromeDriver();
			}
		}
		

		AbstractTestCase.driver.get(url);
		try {
			Thread.sleep(4000);
		} catch (InterruptedException e) {

			e.printStackTrace();
		}
		
		/*LoginPage loginPage = new LoginPage();
		loginPage.loginAs("michelcluz@gmail.com", "4815162342");*/
		
		AbstractTestCase.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		//
		
		AbstractTestCase.driver.manage().timeouts().implicitlyWait(45, TimeUnit.SECONDS);
		System.out.println("Page title is:" + AbstractTestCase.driver.getTitle());
		WebDriverWait wait = new WebDriverWait(AbstractTestCase.driver, 60);


		AbstractTestCase.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		return AbstractTestCase.driver;
	}
	
	public static void takeScreenshot(String name) {
		File scrFile = ((TakesScreenshot) AbstractTestCase.driver).getScreenshotAs(OutputType.FILE);
		String path = AbstractTestCase.path;
		/*
		 * if(path.contains("_15")){ try { FileUtils.deleteDirectory(new
		 * File("AutomatedScreenshots")); } catch (IOException e) {
		 * 
		 * } }
		 */
		try {

			// new File(path).mkdirs();
			String fileName = name + ".jpeg";
			File screenshot = new File(path + "\\Screenshots " + AbstractTestCase.driver.getTitle() + "\\" + fileName);
			System.out.println(path + fileName);
			FileUtils.copyFile(scrFile, screenshot);
		} catch (IOException | IndexOutOfBoundsException e) {
			e.printStackTrace();
		}

	}
	
	public static void takeScreenshot() {
		File scrFile = ((TakesScreenshot) AbstractTestCase.driver).getScreenshotAs(OutputType.FILE);
		String path = AbstractTestCase.path;
		/*
		 * if(path.contains("_15")){ try { FileUtils.deleteDirectory(new
		 * File("AutomatedScreenshots")); } catch (IOException e) {
		 * 
		 * } }
		 */
		try {

			// new File(path).mkdirs();
			String fileName = "Selenium" + Math.random() + ".jpeg";
			File screenshot = new File(path + "\\Screenshots " + AbstractTestCase.driver.getTitle() + "\\" + fileName);
			System.out.println(path + fileName);
			FileUtils.copyFile(scrFile, screenshot);
		} catch (IOException | IndexOutOfBoundsException e) {
			e.printStackTrace();
		}

	}
	
	public static String OSDetector() {
		String os = System.getProperty("os.name").toLowerCase();
		if (os.contains("win")) {
			return "Windows";
		}

		else if (os.contains("nux")) {
			return "Linux";
		} else {
			return "Other";
		}
	}
	
	public static boolean isElementVisible(By by) {
		return getVisibleItems(by).size() > 0;
	}
	
	public static ArrayList<WebElement> getVisibleItems(By by) {
		driver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);
		ArrayList<WebElement> visibleItems = new ArrayList<WebElement>();
		for (WebElement e : driver.findElements(by)) {

			if (e.isDisplayed()) {
				visibleItems.add(e);
			}
		}
		return visibleItems;
	}
	/*public static void waitUntilElementIsPresent(WebElement element, By selector) {
		
		WebDriverWait wait = new WebDriverWait(driver, 30);

		wait.until(ExpectedConditions.elementToBeClickable(element.findElement(selector)));
	}*/
	
}